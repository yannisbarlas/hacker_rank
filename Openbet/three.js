
// if there is no input, return 0
// if the input is not a palindrome, return 0
// if the input is a palindrome, return 1

function sth(input) {
    if (input === undefined || input.length === 0) {
        return 0;
    } else {
        var rev = input.split("").reverse().join("");
        if (input === rev) {
            return 1;
        } else {
            return 0;
        }
    }
    
    
}

console.log(sth("racecar"));
console.log(sth("yes"));