

// in an array where 0 and 1 are the first numbers, and every subsequent number is the sum of
// the two previous ones, write a function that takes as input a number from 1-10 and returns
// the number at that position in the array

function make(n) {
    var arr = [0,1];
    
    for (var i=2 ; i<=10 ; i++) {
        arr.push( (arr[i-1]) + (arr[i-2]) );
    }

    console.log(arr);
    
    return arr[n];
}

console.log(make(3));
