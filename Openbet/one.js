
// find the most frequent letter in a string
// if two have the same frequency, return the one that comes first in the alphabet

function popchar(input) {
    var spl = input.split("");
    var obj = {};
    var often = [];
    var minFreq = 0;
    var min = 'z';
    
    for (var i=0 ; i<spl.length ; i++) {
        if (spl[i] in obj) {
            obj[spl[i]] += 1;
        } else {
            obj[spl[i]] = 1;
        }
    }
    
    for (var key in obj) {
        if (obj[key] > minFreq) {
            minFreq = obj[key];
            often = [key];
        }
        else if (obj[key] === minFreq) {
            often.push(key);
        }
    }
    
    console.log(often);
    
    for ( var j=0 ; j<often.length ; j++) {
        if (often[j] < min) {
            min = often[j];
        }
    }
    
    return min;
}

console.log(popchar("agriculture"));