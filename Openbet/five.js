
// return the score for a string
// spaces don't count
// letters count for their number in the alphabet (a/A=1, c/C=3 etc.)
// for every double letter (adjacent) that a word contains, double the word's score
// input can be a sentence with multiple words

function sth(input) {
    var score = 0;
    var words = input.split(" ");
    var letters = [];
    var multiply = 0;
    
    for (var i=0 ; i<words.length ; i++) {
        var wordScore = 0;
        letters = words[i].split("");
        multiply = 0;
        
        for (var j=0 ; j<letters.length ; j++) {
            wordScore += letters[j].toLowerCase().charCodeAt(0)- 97 +1;
            if (j < letters.length-1 && letters[j] === letters[j+1]) {
                multiply += 1;
            }
        }
        
        for (var k=0 ; k<multiply ; k++) {
            wordScore *= 2;
        }
        
        score += wordScore;
    }
    
    return score;
}


console.log(sth("A"));
console.log(sth("The word needlessly contains two sets of double letters"));
