// You’re given an array containing integer values. 
// You need to print the fraction of count of positive numbers, negative numbers and zeroes to the total numbers. 
// Print the value of the fractions correct to 3 decimal places.

function count (arr) {
    var pos = 0,
        neg = 0,
        zer = 0,
        len = arr.length;
        
    for (var i=0 ; i<len ; i++) {
        var current = arr[i];
        
        if      (current > 0)   { pos += 1; }
        else if (current < 0)   { neg += 1; }
        else if (current === 0) { zer += 1; }
    }
    
    return {
        positive:   parseFloat(pos/len).toFixed(3),
        negative:   parseFloat(neg/len).toFixed(3),
        zero:       parseFloat(zer/len).toFixed(3)
    };
}