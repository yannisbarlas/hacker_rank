// You are given a square matrix of size N×N.
// Calculate the absolute difference of the sums across the two main diagonals.

function calc(n) {
    var sum1 = 0;
    var sum2 = 0;
    var len = n[0].length;

    for (var i=0 ; i<len ; i++) {
        sum1 += n[i][i];
        sum2 += n[i][(len-1)-i];
    }

    return Math.abs(sum1-sum2);
}