// Given a list of unsorted integers, A={a1,a2,…,aN}, can you find the pair of elements 
// that have the smallest absolute difference between them?
// If there are multiple pairs, find them all.

function findDiff(arr) {
    var allMin = [],
        min = 0, 
        currentDiff,
        len = arr.length;
    
    // iterate through all combinations (absolute values means that we only have to look forward in the array)
    for (var i=0 ; i<(len-1) ; i++) {

        for (var j=(i+1) ; j<len ; j++) {

            // get current difference
            currentDiff = Math.abs(arr[i] - arr[j]);

            // if it's the first pair, populate values
            if (i===0 && j===1) {
                min = currentDiff;
                allMin.push( [ arr[i], arr[j] ] );
            } 

            else {
                // if a new smallest difference, update min and reset allMin array
                if (currentDiff < min) {
                    min = Math.abs(arr[i] - arr[j]);
                    allMin = [ [ arr[i], arr[j] ] ];
                }

                // if the same as current smallest min, add pair to allMin array
                else if (currentDiff === min) {
                    allMin.push( [ arr[i], arr[j] ] );
                }

            }
        }
    }
    
    return allMin;
}